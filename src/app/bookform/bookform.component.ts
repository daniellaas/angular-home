import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {
  

  constructor(private booksservice:BooksService, 
              private router:Router, 
              private route:ActivatedRoute,
              public authService:AuthService) { }

  title:string;
  author:string;
  id:string;
  userId:string; 
  isEdit:boolean = false;
  buttonText:string = 'Add book'
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.booksservice.updateBook(this.userId,this.id,this.title,this.author);
    } else {
      this.booksservice.addBooks(this.title,this.author, this.userId)
    }
    this.router.navigate(['/books']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update book'   
          this.booksservice.getbook(this.id,this.userId).subscribe(
            book => {
              console.log(book.data().author)
              console.log(book.data().title)
              this.title = book.data().title; 
              this.author = book.data().author;
              
            }
          )
         }
      }
    )
    
  }

}

