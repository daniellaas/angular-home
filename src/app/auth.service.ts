import { User } from './interfaces/user';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth, private router:Router) {
    this.user = this.afAuth.authState;
  }

  SignUp(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(user=>{
          this.router.navigate(['books']);
          this.router.navigate(['posts']);
        }
        );

  }

  Logout(){
    this.afAuth.auth.signOut();
  }

  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
          user=>{
            this.router.navigate(['books']);
            this.router.navigate(['posts']);
          }
        )
  }
}

