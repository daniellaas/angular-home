import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from './../posts.service';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
  [x: string]: any;

  constructor(private postsservice:PostsService, 
              private router:Router, 
              private route:ActivatedRoute,
              public authService:AuthService) { }

  name:string;
  body:string;
  author:string;
  id:string; 
  userId:string; 
  isEdit:boolean = false;
  buttonText:string = 'Add post'
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.postsservice.updatePost(this.id,this.name,this.body,this.author,this.userId);
    } else {
      this.postsservice.addPost(this.name,this.body,this.author,this.userId)
    }
    this.router.navigate(['/posts']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    console.log(this.id);
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id) {
          this.isEdit = true;
          this.buttonText = 'Update post'   
          this.postsservice.getPost(this.id,this.userId).subscribe(
            post => {
              console.log(post.data().author)
              console.log(post.data().title)
              this.name = post.data().name;
              this.body = post.data().body; 
              this.author = post.data().author;
              
            }
          )
         }
      }
    )
    /*if(this.id) {
      this.isEdit = true;
      this.buttonText = 'Update Post'   
      this.postsservice.getPost(this.id).subscribe(
        post => {
          console.log(post.data().author)
          console.log(post.data().name)
          this.name = post.data().name; 
          this.author = post.data().author;
          this.body = post.data().body; 
          
        }
      )*/
     
  }
  
    
  

}
