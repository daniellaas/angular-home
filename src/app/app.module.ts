import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { TempformComponent } from './tempform/tempform.component';
import { AuthorsComponent } from './authors/authors.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
import { BookformComponent } from './bookform/bookform.component';
import { PostformComponent } from './postform/postform.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';



const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'temperatures/:temp/:city', component: TemperaturesComponent},
  { path: 'bookform', component: BookformComponent},
  { path: 'bookform/:id', component: BookformComponent},
  { path: 'postform', component: PostformComponent},
  { path: 'postform/:id', component: PostformComponent},
  { path: 'authors', component: AuthorsComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'classify', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'login', component: LoginComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    TempformComponent,
    AuthorsComponent,
    PostsComponent,
    BookformComponent,
    PostformComponent,
    SignupComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireAuthModule,
    MatExpansionModule,
    MatCardModule,
    MatFormFieldModule, 
    MatSelectModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    RouterModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )    
  ],
  providers: [PostsService,AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
