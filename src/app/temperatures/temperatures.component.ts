import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

   
  constructor(private route: ActivatedRoute) { }

  likes = 0;
  temperature; 
  city;

  addLikes(){
    this.likes++
  }

  ngOnInit() {
    this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
  }

}
