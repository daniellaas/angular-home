import { Injectable } from '@angular/core';
import {observable, Observable} from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Title } from '@angular/platform-browser';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  /*books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  
  getBooks(){
    const bookObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.books),5000
        )
      }
    )
    return bookObservable;
  }*/
/*
  addBook(){
    setInterval(
      () => this.books.push({title:'A new book', author:'new author'})
    )
  }
*/

userCollection:AngularFirestoreCollection = this.db.collection('users');
bookCollection:AngularFirestoreCollection ;


addBooks(title:string, author:string, userId:string){
  const book = {title:title, author:author}
  /*this.db.collection('books').add(book);*/
  this.userCollection.doc(userId).collection('books').add(book);

}

getbooks(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.bookCollection = this.db.collection(`users/${userId}/books`)
    return this.bookCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
              data.id = document.payload.doc.id;
              console.log(data);
              return data;  
          }
        )
      )
    )
  }

  getbook(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }

deleteBook(id:string,userId:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}

updateBook(userId:string,id:string,title:string, author:string ){
  this.db.doc(`users/${userId}/books/${id}`).update({
    title:title,
    author:author
  })


}

  constructor(private db:AngularFirestore) { 

  }
}
