import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  panelOpenState = false;
  //books: any;
  books$:Observable<any>;
  userId:string;

  constructor(private bookservice:BooksService, public authService:AuthService) { }

  ngOnInit() {
    //this.books$ = this.bookservice.getbooks();
    //this.bookservice.addBooks();

    this.authService.user.subscribe(
      user =>{
        this.userId = user.uid;
        this.books$ =this.bookservice.getbooks(this.userId);
      }
    )
  }

  deleteBook(id:string){
    this.bookservice.deleteBook(id,this.userId);

  }

}
