import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Users } from './../interfaces/users';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  [x: string]: any;
 // [x: string]: any;
  panelOpenState = false;
  userId:string;
  //posts: any;
  posts$:Observable<any[]>;
  /*posts$: Posts[];
  users$: Users[];
  title:String;
  body:String;
  author:string;
  ans:string;*/

  constructor(private postsservice: PostsService,private auth:AuthService) { }

  /* myFunc(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.PostsService.addPosts(this.body,this.author,this.title);   
        }  
      } 
    }
  this.ans ="The data retention was successful"
  }
*/
  ngOnInit() {
    //this.posts$ = this.postsservice.getPosts();
    this.auth.user.subscribe(
      user =>{
        this.userId = user.uid;
        console.log(this.userId);
        this.posts$ =this.postsservice.getPosts(this.userId);
      }
    )
    
  }

  deletePost(id:string){
    this.postsservice.deletePost(id, this.userId);

  }

  

}
