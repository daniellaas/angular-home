import { Injectable } from '@angular/core';
import { HttpClient,HttpClientModule } from '@angular/common/http'
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { PostsRaw } from './interfaces/posts-raw';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection ;

  apiUrl = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrlUser = 'https://jsonplaceholder.typicode.com/users';

  addPosts(body:String, author:String,name:String, userId:string){
    const post = {body:body, author:author, name:name};
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  constructor(private _http: HttpClient,private db:AngularFirestore) {
    
   }

   getPosts(userId:string):Observable<any[]>{
    /*const ref = this.db.collection('posts');
    return ref.valueChanges({idField: 'id'});*/

    this.postCollection = this.db.collection(`users/${userId}/posts`)
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
              data.id = document.payload.doc.id;
              console.log(data);
              return data;  
          }
        )
      )
    )
  }

  

  
  getPost(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get();
  }



   /*getPosts() {
    return this._http.get<Posts[]>(this.apiUrl);}
    getUsers() {
    return this._http.get<Users[]>(this.apiUrlUser);  
    }*/

    addPost(name:string, body:string, author:string, userId:string){
      const post = {name:name, body:body, author:author};
      this.userCollection.doc(userId).collection('posts').add(post);
     // const post = {name:name, author:author, body:body}
    //  this.db.collection('posts').add(post);
    }

    deletePost(id:string ,userId:string){
      this.db.doc(`users/${userId}/posts/${id}`).delete();
    }

    updatePost(id:string,name:string, body:string,author:string, userId:string){
      this.db.doc(`users/${userId}/posts/${id}`).update({
        name:name,
        body:body,
        author:author, 
        
      })
    }

  
}
