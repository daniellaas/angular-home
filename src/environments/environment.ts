// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBlQ7l6knuAuJKH1Ry4arZlx_jM_qlm3oM",
    authDomain: "home-ebe79.firebaseapp.com",
    databaseURL: "https://home-ebe79.firebaseio.com",
    projectId: "home-ebe79",
    storageBucket: "home-ebe79.appspot.com",
    messagingSenderId: "396894997176",
    appId: "1:396894997176:web:c602ca68a2fc732575824d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
